package com.peterwitt.pacman;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.peterwitt.pacman.Game.Enemy;
import com.peterwitt.pacman.Game.GameWorld;
import com.peterwitt.pacman.Game.Player;
import com.peterwitt.pacman.Game.SpawnType;
import com.peterwitt.pacman.Game.Spawner;
import com.peterwitt.pacman.Listeners.OnSwipeTouchListener;
import com.peterwitt.pacman.Listeners.SwipeDirection;
import com.peterwitt.pacman.Listeners.SwipeListener;
import com.peterwitt.pacman.Views.CanvasView;

public class MainActivity extends AppCompatActivity {

    public static int Highscore;
    public static int LastScore = 0;
    public GameWorld gameWorld;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Highscore = getSharedPreferences("pacman_prefs", Context.MODE_PRIVATE).getInt("HS", 0);
        setContentView(R.layout.activity_main);

        gameWorld = new GameWorld(this);
        Player player = new Player("Player");
        gameWorld.addGameObject(player);
        gameWorld.addGameObject(new Spawner(SpawnType.Enemy, 3, 10));
        gameWorld.addGameObject(new Spawner(SpawnType.Pill, 5, 10));

        CanvasView canvasView = findViewById(R.id.Canvas);
        canvasView.setOnTouchListener(new OnSwipeTouchListener(this, player));

        findViewById(R.id.ResetBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gameWorld.shouldReset = true;
            }
        });

        findViewById(R.id.PauseBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gameWorld.togglePause();
            }
        });

    }

    public CanvasView getCanvasView(){
        return (CanvasView) findViewById(R.id.Canvas);
    }

    public void checkHighScore(int score){
        if(score > Highscore)
        {
            Highscore = score;
            getSharedPreferences("pacman_prefs", Context.MODE_PRIVATE).edit().putInt("HS", Highscore).apply();
        }
    }
}
