package com.peterwitt.pacman.Game;
import android.content.Context;
import com.peterwitt.pacman.Game.Interfaces.Collidable;
import com.peterwitt.pacman.Game.Interfaces.Destroyable;
import com.peterwitt.pacman.Game.Interfaces.Resetable;
import com.peterwitt.pacman.Game.Interfaces.Updatable;
import com.peterwitt.pacman.MainActivity;
import java.util.ArrayList;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class GameWorld {

    public static GameWorld instance;

    public int width = 500;
    public int height = 500;

    public Random random;

    public ArrayList<GameObject> GameObjects;
    private ArrayList<GameObject> GameObjectsToDelete;
    private ArrayList<GameObject> GameObjectsToAdd;

    private MainActivity context;

    private Timer gameLoop;
    private long lastTime;
    private float deltatime;
    private boolean paused = false;
    public boolean shouldReset = false;

    public GameWorld(Context context){
        instance = this;
        random = new Random(System.currentTimeMillis());
        this.context = (MainActivity) context;

        gameLoop = new Timer(true);
        gameLoop.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                update();
            }
        }, 0, 33);

        GameObjects = new ArrayList<GameObject>();
        GameObjectsToDelete = new ArrayList<GameObject>();
        GameObjectsToAdd = new ArrayList<GameObject>();
    }

    public void update(){
        //Calcualte time since last frame
        long currTime = System.nanoTime();
        deltatime = (int)(currTime - lastTime) / 1000000000f;
        lastTime = currTime;

        //Update all objects
        for (GameObject go : GameObjects) {
            if(go instanceof Updatable)
                ((Updatable) go).update(paused ? 0 : deltatime);
        }

        //Check collision
        checkCollision();

        //add and remove queued objects
        GameObjects.addAll(GameObjectsToAdd);
        for (GameObject gameObject : GameObjectsToDelete) {
            if(gameObject instanceof Destroyable)
                ((Destroyable) gameObject).OnDestroy();

            GameObjects.remove(gameObject);
        }

        GameObjectsToAdd.clear();
        GameObjectsToDelete.clear();

        //Invalidate the canvas so its redrawn
        context.getCanvasView().postInvalidate();

        if(shouldReset)
            Reset();
    }

    private void checkCollision(){
        //Iterate though all objects that are Collidable
        for (GameObject gameObject : GameObjects) {
            if(gameObject instanceof Collidable){
                Vector2 pos = gameObject.position;
                int colDist = ((Collidable) gameObject).getCollider();

                //Check with all other Collidable objects
                for (GameObject other : GameObjects) {
                    if(other != gameObject && other instanceof Collidable){
                        Vector2 otherPos = other.position;
                        int otherDist = ((Collidable) gameObject).getCollider();
                        int maxDist = Math.max(colDist, otherDist);

                        //If they are Colliding
                        if(circlesColliding(pos, colDist, otherPos, otherDist)){
                            //We have a collision
                            ((Collidable) gameObject).onCollision(other);
                            ((Collidable) other).onCollision(gameObject);
                        }
                    }
                }
            }
        }
    }

    //Returns true if the circles are touching, or false if they are not
    boolean circlesColliding(Vector2 pos1,int radius1, Vector2 pos2,int radius2)
    {
        //compare the distance to combined radii
        float dx = pos2.x -  pos1.x;
        float dy = pos2.y - pos1.y;
        int radii = radius1 + radius2;
        if ( ( dx * dx )  + ( dy * dy ) < radii * radii )
            return true;
        else
            return false;
    }

    public GameObject findObjectWIthTag(String tag){
        for (GameObject gameObject : GameObjects) {
            if(gameObject.tag == tag)
                return gameObject;
        }

        return null;
    }

    public void addGameObject(GameObject toAdd){
        GameObjectsToAdd.add(toAdd);
    }

    public void deleteGameObject(GameObject toDelete){
        GameObjectsToDelete.add(toDelete);
    }

    public void Reset(){
        int playerScore = ((Player)findObjectWIthTag("Player")).score;
        context.checkHighScore(playerScore);
        MainActivity.LastScore = playerScore;

        ArrayList<GameObject> tempList = new ArrayList<>();

        for (GameObject go : GameObjects) {
            if(go instanceof Resetable)
                tempList.add(go);
        }

        for (GameObject gameObject : tempList) {
            if(gameObject instanceof Resetable)
                ((Resetable) gameObject).reset();
        }

        shouldReset = false;
    }

    public void togglePause() {
        paused = !paused;
    }
}
