package com.peterwitt.pacman.Game.Interfaces;
import com.peterwitt.pacman.Game.GameObject;

public interface Collidable {
    int getCollider();
    void onCollision(GameObject other);
}
