package com.peterwitt.pacman.Game.Interfaces;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.View;

public interface Drawable {
    void draw(View view, Canvas canvas, Paint paint);
}
