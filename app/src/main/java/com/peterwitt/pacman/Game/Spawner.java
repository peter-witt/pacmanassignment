package com.peterwitt.pacman.Game;
import com.peterwitt.pacman.Game.Interfaces.Resetable;
import com.peterwitt.pacman.Game.Interfaces.Updatable;

import java.util.ArrayList;

public class Spawner extends GameObject implements Updatable, Resetable{
    private float spawnTimer;
    private float spawnInterval = 1;
    public SpawnType spawnType;
    private int objectCap = 0;

    ArrayList<GameObject> spawnedObjects = new ArrayList<>();

    public Spawner(SpawnType type, float interval){
        spawnType = type;
        spawnInterval = interval;
        objectCap = 0;
    }

    public Spawner(SpawnType type, float interval, int cap){
        spawnType = type;
        spawnInterval = interval;
        objectCap = cap;
        spawnTimer = interval;
    }

    @Override
    public void update(float deltatime) {
        if(spawnTimer <= 0){
            checkSpawn();
            spawnTimer = spawnInterval;
        }
        else
            spawnTimer -= deltatime;
    }

    private void checkSpawn(){
        if(objectCap > 0){
            if(spawnedObjects.size() < objectCap){
                spawnObject();
            }
        }
        else {
            spawnObject();
        }
    }

    private void spawnObject(){
        switch (spawnType){
            case Enemy:
                Enemy e = new Enemy("Enemy");
                e.spawner = this;
                GameWorld.instance.addGameObject(e);
                spawnedObjects.add(e);
                break;
            case Pill:
                Pill p  = new Pill("Pill");
                p.spawner = this;
                GameWorld.instance.addGameObject(p);
                spawnedObjects.add(p);
                break;
        }
    }

    @Override
    public void reset() {
        spawnTimer = spawnInterval;
        for (GameObject obj : spawnedObjects) {
            GameWorld.instance.deleteGameObject(obj);
        }

        spawnedObjects.clear();
    }
}
