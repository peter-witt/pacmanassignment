package com.peterwitt.pacman.Game;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.View;

import com.peterwitt.pacman.Game.Interfaces.Collidable;
import com.peterwitt.pacman.Game.Interfaces.Destroyable;
import com.peterwitt.pacman.Game.Interfaces.Drawable;
import com.peterwitt.pacman.Game.Interfaces.Updatable;
import com.peterwitt.pacman.R;

import java.util.Random;

public class Enemy extends GameObject implements Updatable, Drawable, Collidable, Destroyable{

    private int sprite;
    private int size = 100;
    public Spawner spawner;
    private float speed = 300;
    private Direction moveDirection;
    private Vector2 dir;
    private float speedPrSec = 10;
    private float movementCap = 700;

    public Enemy(String tag){
        this.tag = tag;
        float x = GameWorld.instance.random.nextFloat() * (GameWorld.instance.width - size);
        float y = GameWorld.instance.random.nextFloat() * (GameWorld.instance.height - size);
        position = new Vector2(x, y);



        switch (GameWorld.instance.random.nextInt(4)){
            case 0:
                sprite = R.drawable.ghost_1;
                moveDirection = Direction.Horizontal;
                dir = new Vector2(1, 0);
                break;

            case 1:
                sprite = R.drawable.ghost_2;
                moveDirection = Direction.Vertical;
                dir = new Vector2(0, 1);
                break;

            case 2:
                sprite = R.drawable.ghost_3;
                moveDirection = Direction.Horizontal;
                dir = new Vector2(-1,0);
                break;

            case 3:
                sprite = R.drawable.ghost_4;
                moveDirection = Direction.Vertical;
                dir = new Vector2(0,-1);
                break;
        }
    }

    @Override
    public int getCollider() {
        return 50;
    }

    @Override
    public void onCollision(GameObject other) {
    }

    @Override
    public void draw(View view, Canvas canvas, Paint paint) {
        Bitmap bitmap = BitmapFactory.decodeResource(view.getResources(), sprite);
        canvas.drawBitmap(Bitmap.createScaledBitmap(bitmap, size, size, true), position.x, position.y, paint);
    }

    @Override
    public void update(float deltatime) {
        speed += speedPrSec * deltatime;
        if(speed > movementCap)
            speed = movementCap;

        Vector2 newDir = new Vector2(dir.x, dir.y);
        Translate(newDir.multiply(speed * deltatime));

        switch (moveDirection){
            case Horizontal:
                if(position.x <= 0)
                    dir = new Vector2(1,0);
                else if(position.x > GameWorld.instance.width - size)
                    dir = new Vector2(-1, 0);
                break;
            case Vertical:
                if(position.y <= 0)
                    dir = new Vector2(0,1);
                else if(position.y > GameWorld.instance.height - size)
                    dir = new Vector2(0, -1);
                break;
        }
    }

    @Override
    public void OnDestroy() {
        if(spawner != null)
            spawner.spawnedObjects.remove(this);
    }
}
