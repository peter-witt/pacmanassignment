package com.peterwitt.pacman.Game;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.View;

import com.peterwitt.pacman.Game.Interfaces.Collidable;
import com.peterwitt.pacman.Game.Interfaces.Destroyable;
import com.peterwitt.pacman.Game.Interfaces.Drawable;
import com.peterwitt.pacman.Game.Interfaces.Updatable;
import com.peterwitt.pacman.R;

public class Pill extends GameObject implements Drawable, Collidable, Destroyable{
    public  Spawner spawner;
    public int size = 50;

    public Pill(String tag){
        this.tag = tag;
        float x = GameWorld.instance.random.nextFloat() * (GameWorld.instance.width - size);
        float y = GameWorld.instance.random.nextFloat() * (GameWorld.instance.height - size);
        position = new Vector2(x, y);
    }

    @Override
    public int getCollider() {
        return 10;
    }

    @Override
    public void onCollision(GameObject other) {

    }

    @Override
    public void draw(View view, Canvas canvas, Paint paint) {
        Bitmap bitmap = BitmapFactory.decodeResource(view.getResources(), R.drawable.pill);
        canvas.drawBitmap(Bitmap.createScaledBitmap(bitmap, size, size, true), position.x, position.y, paint);
    }

    @Override
    public void OnDestroy() {
        if(spawner != null)
            spawner.spawnedObjects.remove(this);
    }
}
