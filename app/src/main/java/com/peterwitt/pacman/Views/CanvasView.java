package com.peterwitt.pacman.Views;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.peterwitt.pacman.Game.GameObject;
import com.peterwitt.pacman.Game.GameWorld;
import com.peterwitt.pacman.Game.Interfaces.Drawable;
import com.peterwitt.pacman.Game.Player;
import com.peterwitt.pacman.MainActivity;
import com.peterwitt.pacman.R;

public class CanvasView extends View {

    private Context context;
    private GameObject[] onDrawObjects = new GameObject[0];
    private Paint paint = new Paint();

    public CanvasView(Context context) {
        super(context);
        this.context = context;
    }

    public CanvasView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
    }

    public CanvasView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void onDraw(Canvas canvas){
        //Get screen size and save
        GameWorld.instance.width = canvas.getWidth();
        GameWorld.instance.height = canvas.getHeight();

        //clear entire canvas to white color
        canvas.drawColor(0xff303030);

        //Iterate though objects and draw them
        if(context instanceof MainActivity)
        {
            onDrawObjects = new GameObject[0];
            onDrawObjects = ((MainActivity) context).gameWorld.GameObjects.toArray(onDrawObjects);

            Player player = null;
            for (GameObject onDrawObject : onDrawObjects) {
                if(onDrawObject.tag == "Player"){
                    player = (Player)onDrawObject;
                    break;
                }
            }

            TextView t = ((MainActivity)context).findViewById(R.id.Score);
            t.setText("Score: " + player.score);

            t = ((MainActivity)context).findViewById(R.id.Lives);
            t.setText("Lives: " + player.lives);

            t = ((MainActivity)context).findViewById(R.id.HighScore);
            t.setText("Highscore: " + MainActivity.Highscore);

            t = ((MainActivity)context).findViewById(R.id.LastScore);
            t.setText("Last Score:" + MainActivity.LastScore);

           for(GameObject go : onDrawObjects){
               if(go instanceof Drawable)
                   ((Drawable) go).draw(this, canvas, paint);
           }
        }

        super.onDraw(canvas);
    }
}
